package test;

import org.junit.internal.runners.statements.Fail;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import sample.Main;
import sample.MainTest;

public class TestRunner {
    public static void main(String[] args) {
        Result result = JUnitCore.runClasses(MainTest.class);
        for (Failure failure:result.getFailures()){
            System.out.println(failure.getException());
        }
    }
}
