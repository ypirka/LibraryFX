package sample;

import sample.model.Author;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class DialogController implements Initializable {

    @FXML
    private TextField firstName;
    @FXML
    private TextField middleName;
    @FXML
    private TextField lastName;
    @FXML
    private TextField alias;
    @FXML
    private TextField genre;
    @FXML
    private DatePicker dateBorn;
    @FXML
    private DatePicker dateGone;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public Author getAuthor() {
        String firName = firstName.getText();
        String midName = middleName.getText();
        String lasName = lastName.getText();
        String gnr = genre.getText();
        String als = alias.getText();
        LocalDate born = dateBorn.getValue();
        LocalDate gone = dateGone.getValue();
        Author author = null;
        if (firName != null &&midName!=null&&lasName!=null&&als!=null&& gnr != null && born != null) {
            author = new Author(-1,firName,midName,lasName,als, gnr, born, gone);
        }
        return author;
    }
}
