package sample;

import sample.model.Author;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.util.Optional;

public class CreateAuthorDialog extends Dialog<Author> {


    public CreateAuthorDialog(Main main) {
        setTitle("Login Dialog");
        setHeaderText("Look, a Custom Login Dialog");
        ButtonType loginButtonType = new ButtonType("Create", ButtonBar.ButtonData.OK_DONE);
        getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("layout/authorDialog.fxml"));
        AnchorPane root = null;
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        DialogController controller = loader.getController();

        getDialogPane().setContent(root);
        setResultConverter(param -> {
            System.out.println("Converter");
            Author author = controller.getAuthor();
            return author;
        });
        Optional<Author> result = showAndWait();
        result.ifPresent(author -> {
            if (author == null) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Complete all fields");
                alert.show();
            } else {
                main.addAuthorToData(author);
            }
        });
    }
}
