package sample;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import sample.datebase.DatabaseConnector;
import sample.model.Author;
import sample.model.Book;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class SampleController implements Initializable {

    @FXML
    private TableColumn<Book, String> libraryTitleColumn;
    @FXML
    private TableColumn<Book, String> libraryAuthorColumn;
    @FXML
    private TableColumn<Book, LocalDate> libraryDateColumn;
    @FXML
    private TableView<Book> libraryTableView;
    @FXML
    private DatePicker year;
    @FXML
    private TextArea description;
    @FXML
    private ComboBox<Author> authorCBox;
    @FXML
    private TextField title;


    private AddAuthorListener addAuthorListener;
    private AddBookListener addBookListener;

    private ObservableList<Book> books;

    private ObservableList<Author> authors;

    public SampleController() {
        System.out.println("Controller constructor");
    }

    public void showTableView() {
        libraryTitleColumn.setCellValueFactory(new PropertyValueFactory<Book, String>("title"));
        libraryAuthorColumn.setCellValueFactory(new PropertyValueFactory<Book, String>("author"));
        libraryDateColumn.setCellValueFactory(new PropertyValueFactory<Book, LocalDate>("releaseDate"));
        libraryTableView.setItems(books);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        System.out.println("SampleController initialise");
    }

    public void newBook(ActionEvent actionEvent) {
        title.clear();
        year.getEditor().clear();
        authorCBox.getSelectionModel().clearSelection();
        description.clear();
    }

    public void save(ActionEvent actionEvent) {
        String titleText = title.getText();
        Author authorText = authors.get(authorCBox.getSelectionModel().getSelectedIndex());
        LocalDate yearTemp = year.getValue();
        Book book = new Book(-1,authorText, titleText,"0001"," ",
                yearTemp,"wow");
        addBookListener.addBook(book);
    }

    public void setAuthors(ObservableList<Author> authors) {
        this.authors = authors;
        authorCBox.setItems(this.authors);
    }

    public void setBooks(ObservableList<Book> books) {
        System.out.println("set books");
        this.books = books;
        showTableView();
    }

    public void setAddBookListener(AddBookListener addBookListener) {
        this.addBookListener = addBookListener;
    }

    public void setAddAuthorListener(AddAuthorListener addAuthorListener) {
        this.addAuthorListener = addAuthorListener;
    }

    public void addAuthor(ActionEvent actionEvent) {
        addAuthorListener.showAuthorDialog();
    }

    public void exportDataBase(ActionEvent actionEvent) {
        DatabaseConnector.getInstance().exportDataBase();
    }

    public void importDataBase(ActionEvent actionEvent) {
        DatabaseConnector.getInstance().importDataBase();
    }


    public interface AddAuthorListener {
        void showAuthorDialog();
    }

    public interface AddBookListener {
        void addBook(Book book);
    }
}
