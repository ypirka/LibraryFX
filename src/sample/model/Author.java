package sample.model;

import java.time.LocalDate;

public class Author {
    private long id;
    private String firstName;
    private String middleName;
    private String lastName;
    private String alias;
    private String genre;
    private LocalDate birthDay;
    private LocalDate dateGone;
    private static long idCounter;


    public long getId() {
        return id;
    }

    public Author(long id, String firstName, String middleName, String lastName, String alias, String genre, LocalDate birthDay, LocalDate dateGone) {
        this.id = id;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.alias = alias;
        this.genre = genre;
        this.birthDay = birthDay;
        this.dateGone = dateGone;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAlias() {
        return alias;
    }

    public String getGenre() {
        return genre;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    public LocalDate getDateGone() {
        return dateGone;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return firstName;
    }

    public String getFirstName() {
        return firstName;
    }
}
