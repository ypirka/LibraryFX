package sample.model;

import java.time.LocalDate;

public class Book {

    private long id;
    private Author author;
    private String title;
    private String isbn;
    private String coverImage;
    private LocalDate releaseDate;
    private String description;

    public Book(long id, Author author, String title, String isbn,
                String coverImage, LocalDate releaseDate,
                String description) {
        this.id = id;
        this.author = author;
        this.title = title;
        this.isbn = isbn;
        this.coverImage = coverImage;
        this.releaseDate = releaseDate;
        this.description = description;
    }

    public String getAuthor() {
        return author.getFirstName();
    }

    public String getTitle() {
        return title;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

}
