package sample;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import sample.datebase.DatabaseConnector;
import sample.model.Author;
import sample.model.Book;

import java.time.LocalDate;

public class Main extends Application implements SampleController.AddAuthorListener, SampleController.AddBookListener {
    private ObservableList<Book> books = FXCollections.observableArrayList();
    private ObservableList<Author> authors = FXCollections.observableArrayList();

    public Main() {
        System.out.println("Main constructor");
        DatabaseConnector databaseConnector = DatabaseConnector.getInstance();
        authors.addAll(databaseConnector.getAuthorsList());
        books.addAll(databaseConnector.getBooksList());

    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        System.out.println("Application start");

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("layout/sample.fxml"));
        AnchorPane root = loader.load();
        SampleController controller = loader.getController();
        controller.setBooks(books);
        controller.setAuthors(authors);
        controller.setAddAuthorListener(this);
        controller.setAddBookListener(this);


        primaryStage.setTitle("Library");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
        System.out.println("Application close");
    }

    public ObservableList<Author> getAuthors() {
        return authors;
    }

    public void addAuthorToData(Author author){
     authors.add(DatabaseConnector.getInstance().addAuthorToDatabase(author));
    }

    @Override
    public void showAuthorDialog() {
        new CreateAuthorDialog(this);
    }

    @Override
    public void addBook(Book book) {
        books.add(book);
    }
}
