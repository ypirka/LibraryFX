package sample.datebase;

public class InlineOutlineImportExport implements DatabaseImportExport {

    @Override
    public boolean importDatabase() {

        return false;
    }

    @Override
    public boolean exportDatabase() {
        String exportFile;
        exportFile = "select * from authors into outfile '" +
                DatabaseConnector.getInstance().getPath().replaceAll("\\\\", "/") +
                "authors_J1017_1.csv';";
        DatabaseConnector.getInstance().execute(exportFile);

        exportFile = "select * from books into outfile '" +
                DatabaseConnector.getInstance().getPath().replaceAll("\\\\", "/") +
                "books_J1017_1.csv';";
        DatabaseConnector.getInstance().execute(exportFile);

        exportFile = "select * from firstnames into outfile '" +
                DatabaseConnector.getInstance().getPath().
                        replaceAll("\\\\", "/") +
                "firstnames_J1017_1.csv';";
        DatabaseConnector.getInstance().execute(exportFile);

        exportFile = "select * from lastnames into outfile '" +
                DatabaseConnector.getInstance().getPath().replaceAll("\\\\", "/") +
                "lastnames_J1017_1.csv';";
        DatabaseConnector.getInstance().execute(exportFile);

        exportFile = "select * from middlenames into outfile '" + DatabaseConnector.getInstance().
                getPath().replaceAll("\\\\", "/") +
                "middlenames_J1017_1.csv';";
        DatabaseConnector.getInstance().execute(exportFile);

        return false;
    }
}
