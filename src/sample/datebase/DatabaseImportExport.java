package sample.datebase;

public interface DatabaseImportExport {
    public boolean importDatabase();
    public boolean exportDatabase();
}
