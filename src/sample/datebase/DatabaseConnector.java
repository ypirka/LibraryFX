package sample.datebase;

import sample.log.TinyLog;
import sample.model.Author;
import sample.model.Book;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DatabaseConnector {
    private DatabaseImportExport databaseImportExport;
    private static DatabaseConnector instance = null;
    private static final String DRIVER_CLASS = "com.mysql.jdbc.Driver";

    public static final String URL = "jdbc:mysql://localhost:3306";
    private static final String USER = "root";
    public static final String PASSWORD = "admin";
    public static final String NAME_DATABASE = "bestlibrary";

    private Statement statement;
    private String path;
    private List<Author> authorsList;
    private List<Book> booksList;

    private DatabaseConnector() {
        try {
            Class.forName(DRIVER_CLASS);
            Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
            statement = connection.createStatement();
            statement.execute("use " + NAME_DATABASE + ";");
            ResultSet showVar = statement.executeQuery
                    ("SHOW VARIABLES LIKE \"secure_file_priv\";");
            showVar.next();
            path = showVar.getString("Value");
            TinyLog.trace("Path : " + path);
            ResultSet resultSet = statement.executeQuery("show tables;");
            while (resultSet.next()) {
                System.out.println(resultSet.getString("Tables_in_bestlibrary"));
            }
        } catch (ClassNotFoundException e) {
            TinyLog.warn("ClassNotFound: " + e.getMessage());
        } catch (SQLException e) {
            TinyLog.warn("Sql error: " + e.getMessage());
        }
        loadData();
    }

    public static DatabaseConnector getInstance() {
        synchronized (DatabaseConnector.class) {
            if (instance == null) {
                instance = new DatabaseConnector();
                instance.databaseImportExport = new InlineOutlineImportExport();
            }
        }
        return instance;

    }

    public ResultSet executeQuery(String command) {
        try {
            return statement.executeQuery(command);
        } catch (SQLException e) {
            TinyLog.warn("ExecuteQuery Error: " + e.getMessage());
            return null;
        }
    }

    public boolean execute(String command) {
        try {
            return statement.execute(command);
        } catch (SQLException e) {
            TinyLog.warn("Execute Error: " + e.getMessage());
            return false;
        }
    }

    public void exportDataBase() {
        databaseImportExport.exportDatabase();
    }

    public void importDataBase() {
        databaseImportExport.importDatabase();
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    private void loadAuthors() {
        authorsList = new ArrayList<>();
        String sql = "select authors.id, firstnames.firstName,\n" +
                "middlenames.middleName,lastnames.lastName,\n" +
                "authors.dateBirth,authors.goneDate,\n" +
                "authors.alias,authors.genre \n" +
                " from authors join firstnames \n" +
                " on authors.firstName = firstnames.id \n" +
                "  join middlenames \n" +
                "  on authors.middleName = middlenames.id\n" +
                "   join lastnames \n" +
                "\t on authors.lastName = lastnames.id;";

        try {
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                long id;
                String firstName, middleName, lastName,
                        alias, genre;
                Date dateBirth, goneDate;
                id = resultSet.getLong("id");
                firstName = resultSet.getString("firstName");
                middleName = resultSet.getString("middleName");
                lastName = resultSet.getString("lastName");
                alias = resultSet.getString("alias");
                genre = resultSet.getString("genre");
                dateBirth = resultSet.getDate("dateBirth");
                goneDate = resultSet.getDate("goneDate");
                Author author = new Author(id, firstName, middleName, lastName,
                        alias, genre, dateBirth.toLocalDate(),
                        goneDate.toLocalDate());
                authorsList.add(author);
            }
        } catch (SQLException e) {
            TinyLog.error(e.getMessage());
        }
    }

    private void loadBooks() {
        booksList = new ArrayList<>();
        String sql = "select books.id, books.title, \n" +
                "books.description, books.realease_date,\n" +
                "books.coverLink, books.volume, books.isbn,\n" +
                "books.authorId\n" +
                " from books;";
        try {
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                long id, authorId;
                String title, description, coverLink, isbn;
                int volume;
                Date releaseDate;
                id = resultSet.getLong("id");
                title = resultSet.getString("title");
                description = resultSet.getString("description");
                coverLink = resultSet.getString("coverLink");
                isbn = resultSet.getString("isbn");
                volume = resultSet.getInt("volume");
                releaseDate = resultSet.getDate("realease_date");
                Author author = getAuthorById(resultSet.getLong
                        ("authorId"));
                Book book = new Book(id, author, title, isbn, coverLink,
                        releaseDate.toLocalDate(), description);
                booksList.add(book);
            }
        } catch (SQLException e) {
            TinyLog.error(e.getMessage());
        }
    }

    public Author getAuthorById(long id) {

        for (Author a : authorsList) {
            if (a.getId() == id) {
                return a;
            }
        }

        return null;
    }

    public void loadData() {
        loadAuthors();
        loadBooks();
    }

    public List<Author> getAuthorsList() {
        return authorsList;
    }

    public List<Book> getBooksList() {
        return booksList;
    }

    public long getIdFirstName(String firstName){
        String sql ="select id from firstnames " +
                "where firstname like";
        String insert="insert into firstnames (firstname) " +
                "values";
        try {
            ResultSet resultSet=statement.executeQuery(sql+" '"+firstName+"';");
            if(resultSet.first()){
               return resultSet.getLong("id");
            }else {
                statement.executeUpdate(insert+" ('"+firstName+"');");
                ResultSet resultSet1=statement.executeQuery(sql+" '"+firstName+"';");
                resultSet1.first();
                return  resultSet1.getLong("id");
            }
        } catch (SQLException e) {
            TinyLog.trace("SQL Error(firstName): "+e.getMessage());
        }
        TinyLog.warn("FirstNameId not found");
        return -1;
    }
    public  long getIdMiddleName(String middleName){
        String sql="select id from middlenames where middlename like";
        String insert="insert into middlenames (middlename) values";
        try {
            ResultSet resultSet=statement.executeQuery(sql+" '"+middleName+"';");
            if(resultSet.first()){
                return resultSet.getLong("id");
            }else {
                statement.executeUpdate(insert+" ('"+middleName+"');");
                ResultSet resultSet1 = statement.executeQuery(sql+" '"+middleName+"';");
                resultSet1.first();
                return resultSet1.getLong("id");
            }
        } catch (SQLException e) {
            TinyLog.trace("SQL Error(MiddleName): "+e.getMessage());
        }
        TinyLog.error("MiddleNameId not found");
        return -1;
    }
    public long getIdLastName(String lastName){
        String sql ="select id from lastnames where lastname like";
        String insert="insert into lastnames (lastname) values";
        try {
            ResultSet resultSet = statement.executeQuery(sql+" '"+lastName+"';");
            if (resultSet.first()) {
                return resultSet.getLong("id");
            }else {
                statement.executeUpdate(insert+" ('"+lastName+"');");
                ResultSet resultSet1 = statement.executeQuery(sql+" '"+lastName+"';");
                resultSet1.first();
                return resultSet1.getLong("id");
            }
        } catch (SQLException e) {
            TinyLog.trace("SQL Error(LastName): "+e.getMessage());
        }
        TinyLog.error("LastNameId not found");
        return -1;
    }
    public Author addAuthorToDatabase(Author author){
        String sql ="select id from author where lastname like";
        String insert="insert into authors (" +
                "firstName," +
                "middleName," +
                "lastName," +
                "dateBirth," +
                "goneDate," +
                "alias," +
                "genre)" +
                "values";
        String select = "select authors.id from authors " +
                "join firstnames on authors.firstName=firstnames.id " +
                "join middlenames on authors.middleName=middlenames.id " +
                "join lastnames on authors.lastName=lastnames.id " +
                "where (firstnames.firstname like '"+author.getFirstName()+"') and " +
                "(middlenames.middleName like '"+author.getMiddleName()+"') and " +
                "(lastnames.lastName like '"+author.getLastName()+"') and " +
                "(authors.alias like '"+author.getAlias()+"') and " +
                "(authors.dateBirth like '"+author.getBirthDay()+ "'); ";
        try {
            statement.executeUpdate(insert+"('"+getIdFirstName(author.getFirstName())+"'," +
                    "'"+getIdMiddleName(author.getMiddleName())+"',"+"'"+getIdLastName(author.getLastName())+"',"+
                "'"+author.getBirthDay()+"',"+"'"+author.getDateGone()+"',"+
            "'"+author.getAlias()+"',"+"'"+author.getGenre()+"');");
            ResultSet resultSet= statement.executeQuery(select);
            resultSet.first();
            author.setId(resultSet.getLong("id"));
        } catch (SQLException e) {
            TinyLog.trace("Author not be created: "+e.getMessage());
        }

        return author;
    }
}
