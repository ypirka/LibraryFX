alter table authors change alias
alias bigint unsigned not null;
alter table authors change firstName
firstName bigint unsigned not null;
alter table authors change middleName
middleName bigint unsigned not null;
alter table authors change lastName
lastName bigint unsigned not null;