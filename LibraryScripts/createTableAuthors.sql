create TABLE authors (
	id serial,
	firstName varchar(20) NOT NULL,
	middleName varchar(40),
	lastName varchar(40),
	dateBirth DATE,
	goneDate DATE,
	alias varchar(20),
	genre varchar(50),
	PRIMARY KEY (id)	   	
) CHARSET UTF8  ; 