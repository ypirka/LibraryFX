create TABLE books (
	id serial,
	title varchar(100) NOT NULL,
	description text,
	realeaseDate DATE,
	coverLink varchar(150),
	volume INT,
	isbn varchar(24),
	authorId BIGINT UNSIGNED NOT NULL ,
	PRIMARY KEY (id), 
	FOREIGN KEY (authorId) REFERENCES authors(id)
	)	   	
 CHARSET UTF8  ; 