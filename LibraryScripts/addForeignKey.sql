ALTER TABLE authors ADD FOREIGN KEY (firstname) REFERENCES names (id);
ALTER TABLE authors ADD FOREIGN KEY (middlename) REFERENCES names (id);
ALTER TABLE authors ADD FOREIGN KEY (lastname) REFERENCES names (id);
ALTER TABLE authors ADD FOREIGN KEY (alias) REFERENCES names (id);