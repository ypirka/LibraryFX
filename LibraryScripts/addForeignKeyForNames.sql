alter table authors add 
foreign key (firstName) 
references names (id);
alter table authors add 
foreign key (middleName) 
references names (id);

alter table authors add 
foreign key (lastName) 
references names (id);

alter table authors add 
foreign key (alias) 
references names (id);