alter table authors 
change alias alias varchar(50),
add foreign key (firstName) 
 references firstNames(id),
add foreign key (middleName)
 references middleNames(id),
add foreign key (lastName)
 references lastNames(id);   