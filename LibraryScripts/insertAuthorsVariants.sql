insert into authors(firstName,middleName,lastName,dateBirth,goneDate,alias)
values (1,1,1,'1858-10-2','1912-10-2','L.T');
insert into authors(firstName,middleName,lastName,dateBirth,goneDate)
values ((select id from firstnames 
	WHERE firstname ='Dante'),
	(select id from middlenames 
	WHERE middlename ='Delie'),
	(select id from lastnames 
	WHERE lastname ='Alegeri'),
	'1258-10-2','1321-10-2');
  
insert into authors(firstName,middleName,lastName,dateBirth,goneDate)
values ((select id from firstnames 
	WHERE firstname like '%Vasia%'),
	(select id from middlenames 
	WHERE middlename like '%Igorevich%'),
	(select id from lastnames 
	WHERE lastname like '%Pupkin%'),
	'1258-10-2','1321-10-2');
  