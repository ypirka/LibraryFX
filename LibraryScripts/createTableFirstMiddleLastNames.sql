create table firstNames(
id serial,
firstName varchar(50) unique  not null,
primary key (id)
) charset utf8;
create table middleNames(
id serial,
middleName varchar(50) unique  not null,
primary key (id)
) charset utf8;
create table lastNames(
id serial,
lastName varchar(50) unique  not null,
primary key (id)
) charset utf8;